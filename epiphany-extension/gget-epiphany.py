#!/usr/bin/env python2.5
# -*- coding: utf-8 -*-

# Copyright (C) 2008 Johan Svedberg <johan@svedberg.com>

# This file is part of GGet.

# GGet is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# GGet is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with GGet; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import os

import dbus
import epiphany

SERVICE = "org.gnome.GGet"
MAIN_WINDOW_OBJ_PATH = "/org/gnome/GGet/MainWindow"
DOWNLOAD_MGR_OBJ_PATH = "/org/gnome/GGet/DownloadManager"

class GGetEpiphanyPlugin():
    def __init__(self):
        self.shell = epiphany.ephy_shell_get_default()
        self.embed_single = self.shell.get_embed_single()
        self.embed_single.connect("handle-content", self.__handle_content)

    def __handle_content(self, shell, mime_type, uri):
        if not uri:
            return False

        starter_bus = dbus.StarterBus()
        try:
            main_window = starter_bus.get_object(SERVICE, MAIN_WINDOW_OBJ_PATH)
            download_manager = starter_bus.get_object(SERVICE,
                                                      DOWNLOAD_MGR_OBJ_PATH)
        except Exception, e:
            return False

        main_window.Present()
        download_manager.AddDownload(uri, os.getcwd())
        return True

plugin = GGetEpiphanyPlugin()

# vim: set sw=4 et sts=4 tw=79 fo+=l:
